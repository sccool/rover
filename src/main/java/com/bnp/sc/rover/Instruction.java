package com.bnp.sc.rover;



import java.util.regex.Pattern;

public class Instruction {
	private Type type;
	private String value;

	public enum Type {
		AREA_LIMIT, ROVER, ROUTE, UNKNOWN;
	}

	public Instruction(String value) {
		this.value = value;

		if (Pattern.matches("[LRM]+", value)) {
			type = Type.ROUTE;
		} else {
			if (Pattern.matches("\\d\\s\\d", value)) {
				type = Type.AREA_LIMIT;
			} else {

				if (Pattern.matches("\\d\\s\\d\\s[NSEW]", value)) {
					type = Type.ROVER;
				}
			}

		}

	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}