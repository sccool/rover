package com.bnp.sc.rover;

import java.util.HashMap;
import java.util.Map;

public class Compass {

	// cardinal map
	// define closest left cardinal (0) and right cardinal(1)
	// for all 4 cardinal
	private final static Map<Cardinal, Cardinal[]> QUADRANT = new HashMap<Cardinal, Cardinal[]>();

	//initialize QUADRANTS
	static {
		QUADRANT.put(Cardinal.NORTH, new Cardinal[] { Cardinal.WEST, Cardinal.EST });
		QUADRANT.put(Cardinal.EST, new Cardinal[] { Cardinal.NORTH, Cardinal.SOUTH });
		QUADRANT.put(Cardinal.SOUTH, new Cardinal[] { Cardinal.EST, Cardinal.WEST });
		QUADRANT.put(Cardinal.WEST, new Cardinal[] { Cardinal.SOUTH, Cardinal.NORTH });
	}

	//Movement on compass
	public enum Move {
		LEFT, RIGHT;
	}

	//current cardinal position
	private Cardinal current;

	public Compass(Cardinal initial) {
		this.current = initial;

	}

	public void move(Move move) {

		if (Move.LEFT.equals(move)) {
			current = QUADRANT.get(current)[0];
		} else {
			current = QUADRANT.get(current)[1];
		}
	}

	public Cardinal getCurrentOrientation() {
		return current;
	}

}
