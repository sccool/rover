package com.bnp.sc.rover;

/**
 * 
 * Planet mars where the squad launched
 * 
 * @author sc
 *
 */
public class Mars {

	public static void main(String[] args) throws Exception {
		if (args == null || args.length == 0) {
			System.out.println("no instructions sended by the NASA");
			System.exit(0);
		}

		// the new squad on mars (the rover group)
		Squad squad = new Squad();

		// the squad receive the NASA Instructions
		squad.receive(InstructionFactory.extract(args[0]));

		// show squad informations
		System.out.println(squad.toString());

		// there are a lost rover in Area?
		for (Rover rover : squad.getRovers()) {
			if (!squad.getArea().isInLimit(rover)) {
				System.out.println("over limit : rover disappeared");
			}
		}

	}

}
