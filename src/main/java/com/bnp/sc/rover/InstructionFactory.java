package com.bnp.sc.rover;



import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;


/***
 * 
 * Instruction factory to build
 * 
 * @author sc
 *
 */
public class InstructionFactory {

	//static method to extract instruction on file path
	public static List<Instruction> extract(String filePath) throws Exception {
		List<Instruction> instructions = Files.lines(Paths.get(filePath)).map(Instruction::new)
				.collect(Collectors.toList());
		return instructions;
	}

}
