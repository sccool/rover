package com.bnp.sc.rover;




/**
 * 
 * zone to delimit rover zone
 * 
 * @author sc
 *
 */
public class Area {

	Position leftCorner = new Position(0,0);
	
	Position rightCorner;
	
	public Area(String value) {
		String[] posValues = value.split(" ");
		this.rightCorner = new Position(Integer.valueOf(posValues[0]),Integer.valueOf(posValues[1]));
	}

	public Area(Position rightCorner) {
		super();
		this.rightCorner = rightCorner;
	}
	
	

	public boolean isInLimit(Rover rover) {
		return leftCorner.getX()<=rover.getPosition().getX() && rover.getPosition().getX()<=rightCorner.getX() 
				&& leftCorner.getY()<=rover.getPosition().getY() && rover.getPosition().getY()<=rightCorner.getY();
	}

	public Position getLeftCorner() {
		return leftCorner;
	}

	public void setLeftCorner(Position leftCorner) {
		this.leftCorner = leftCorner;
	}

	public Position getRightCorner() {
		return rightCorner;
	}

	public void setRightCorner(Position rightCorner) {
		this.rightCorner = rightCorner;
	}

}
