package com.bnp.sc.rover;

import com.bnp.sc.rover.Compass.Move;

/**
 * 
 * Rover robot
 * 
 * @author sc
 *
 */
public class Rover {

	//the position of the rover 
	private Position position;
	
	//the compass to calculate the orientation of the rover
	private Compass compass;
	
	
	/***
	 * 
	 * Constructor 
	 * 
	 * @param value represent informations position and direction
	 */
	public Rover(String value) {
		String[] posCardValues = value.split(" ");	
		this.position = new Position(Integer.valueOf(posCardValues[0]),Integer.valueOf(posCardValues[1]));
		this.compass = new Compass(Cardinal.findByShortName(posCardValues[2]));
		
		
	}

	public Rover(Position position, Cardinal direction) {
		this.compass = new Compass(direction);
		this.position = position;
	}

	public void move() {
		switch (compass.getCurrentOrientation()) {
		case NORTH:
			position.plusY();
			break;

		case EST:
			position.plusX();
			break;

		case SOUTH:
			position.minusY();
			break;

		case WEST:
			position.minusX();
			break;

		default:
			break;
		}
		
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Compass getCompass() {
		return compass;
	}

	public void setCompass(Compass compass) {
		this.compass = compass;
	}

	public void spinLeft() {
		//if the rove turn to left move the compass to LEFT
		compass.move(Move.LEFT);
	}

	public void spinRight() {
		//if the rove turn to left move the compass to RIGHT
		compass.move(Move.RIGHT);

	}

	//the rover forward
	public void move(String route) {
		for (int i = 0; i < route.length(); i++) {
			char letter = route.charAt(i);

			switch (letter) {
			case 'L':
				spinLeft();
				break;
			case 'R':
				spinRight();
				break;
			default:
				move();
				break;
			}

		}
	}

	@Override
	public String toString() {
		return "Rover [x=" + position.getX() + ", y=" + position.getY() + ", "
				+ compass.getCurrentOrientation().getShortName() + "]";
	}


}
