package com.bnp.sc.rover;

/***
 * 
 * Cardinal point
 * 
 * @author sc
 *
 */
public enum Cardinal {
	
	NORTH("N"), EST("E"), SOUTH("S"), WEST("W");

	private String shortName;

	private Cardinal(String key) {
		this.shortName = key;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public static Cardinal findByShortName(String value) {

		for (Cardinal cardinal : Cardinal.values()) {
			if (cardinal.getShortName().equals(value)) {
				return cardinal;
			}
		}
		return null;

	}

}
