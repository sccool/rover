package com.bnp.sc.rover;

/**
 * 
 * Coordinates point in space
 * 
 * @author sc
 *
 */
public class Position {
	
	private Integer x;
	
	private Integer y;

	public Position(Integer x, Integer y) {
		super();
		this.x = x;
		this.y = y;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public void plusY() {
		this.y++;
		
	}

	public void plusX() {
		this.x++;
		
	}

	public void minusY() {
		this.y--;
	}
	
	public void minusX() {
		this.x--;
	}

	@Override
	public String toString() {
		return "Position [x=" + x + ", y=" + y + "]";
	}
	
	

}
