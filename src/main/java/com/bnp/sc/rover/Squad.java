package com.bnp.sc.rover;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Represent group of rover
 * 
 * @author sc
 *
 */
public class Squad {

	//the delimited of the squad rover
	private Area area;

	private List<Rover> rovers = new ArrayList<Rover>();

	//the current or last activate rover in squad
	private Rover currentActivated;

	public void receive(List<Instruction> instructions) {

		for (Instruction instruction : instructions) {
			switch (instruction.getType()) {
			case AREA_LIMIT:
				area = new Area(instruction.getValue());
				break;

			case ROVER:
				currentActivated = new Rover(instruction.getValue());
				rovers.add(currentActivated);
				break;

			case ROUTE:
				currentActivated.move(instruction.getValue());
				break;

			default:
				System.out.println("unknown instruction");
				break;
			}
		}

	}

	
	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public List<Rover> getRovers() {
		return rovers;
	}

	public void setSquad(List<Rover> rovers) {
		this.rovers = rovers;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for (Rover rover : rovers) {
			String value = String.format("%s %s %s\n",rover.getPosition().getX(),rover.getPosition().getY(),rover.getCompass().getCurrentOrientation().getShortName());
			buffer.append(value);
		}
		
		return buffer.toString();
		
	}
	
	


}
