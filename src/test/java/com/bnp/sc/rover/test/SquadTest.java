package com.bnp.sc.rover.test;



import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.junit.Test;

import com.bnp.sc.rover.Instruction;
import com.bnp.sc.rover.InstructionFactory;
import com.bnp.sc.rover.Squad;

public class SquadTest {

	@Test
	public void receive_instruction() throws Exception {
		List<Instruction> instructions = InstructionFactory
				.extract(new File("src/test/resources/inputs.txt").getAbsolutePath());
	
		Squad squad = new Squad();
		squad.receive(instructions);
		System.out.println(squad.toString());
		
		assertTrue("Rover [x=1, y=3, N]".equals(squad.getRovers().get(0).toString()));
		assertTrue("Rover [x=5, y=1, E]".equals(squad.getRovers().get(1).toString()));
		
	}

}
