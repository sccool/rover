package com.bnp.sc.rover.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.bnp.sc.rover.Cardinal;
import com.bnp.sc.rover.Position;
import com.bnp.sc.rover.Rover;

public class RoverTest {
	
	
	@Test
	public void startToPoint_1_1_North() {
		Rover rover = new Rover(new Position(1,1), Cardinal.NORTH);
		assertTrue("Rover [x=1, y=1, N]".equals(rover.toString())); 
	}
	
	@Test
	public void startToPoint_2_1_North_turn_to_left_and_move_endToPoint_1_1() {
		Rover rover = new Rover(new Position(2,1), Cardinal.NORTH);
		rover.spinLeft();
		rover.move();
		assertTrue("Rover [x=1, y=1, W]".equals(rover.toString())); 
	}
	
	@Test
	public void startToPoint_1_1_North_by_string_value() {
		Rover rover = new Rover("1 1 N");
		assertTrue("Rover [x=1, y=1, N]".equals(rover.toString())); 
	}

}