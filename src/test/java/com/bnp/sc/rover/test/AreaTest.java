package com.bnp.sc.rover.test;



import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.bnp.sc.rover.Area;
import com.bnp.sc.rover.Position;
import com.bnp.sc.rover.Rover;

public class AreaTest {
	
	@Test
	public void create_area_with_string_value() {
		Area area = new Area("2 2");
		Rover rover = new Rover("1 2 N");
		assertTrue(area.isInLimit(rover));
	}
	
	@Test
	public void rover_not_in_area() {
		Area area = new Area(new Position(2, 2));
		Rover rover = new Rover("-1 2 N");
		assertFalse(area.isInLimit(rover));
	}
	
	@Test
	public void rover_in_area() {
		Area area = new Area(new Position(2, 2));
		Rover rover = new Rover("1 2 N");
		assertTrue(area.isInLimit(rover));
	}

}
